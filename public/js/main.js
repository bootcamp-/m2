document.getElementById("darkModeSwitch").onchange = function(e){
    const htmlTag = document.querySelector("html");
    if (e.target.checked) {
        htmlTag.setAttribute("data-bs-theme", "dark");
    }
    else {
        htmlTag.setAttribute("data-bs-theme", "light");
    }
};

document.getElementById("fullViewSwitch").onchange = function(e){
    const cardPicture = document.getElementsByClassName("card-img-top");
    if (e.target.checked) {
        for (i of cardPicture) {
            i.style.display = "flex";
        }
    }
    else {
        for (i of cardPicture) {
            i.style.display = "none";
        }
    }
};

const mainModal = document.getElementById("mainModal");
if (mainModal) {
    mainModal.addEventListener("show.bs.modal", event => {
        const button = event.relatedTarget;
        const digiName = button.getAttribute("data-bs-nombre");
        const digiLevel = button.getAttribute("data-bs-nivel");
        const digiImage = button.getAttribute("data-bs-imagen");

        const mainModalTitle = mainModal.querySelector(".modal-title");
        const mainModalImage = mainModal.querySelector("#mainModalImage");
        
        mainModalTitle.innerHTML = `
            <p class="fs-2">${digiName} <small>(${digiLevel})</small></p>
        `;
        mainModalImage.setAttribute("src", `${digiImage}`);
    })
}

var digimon = "https://digimon-api.vercel.app/api/digimon";
var mainContainer = document.querySelector("#mainContainer");

fetch(digimon)
.then(response => response.json())
.then(data => {
    for (i of data) {
        mainContainer.innerHTML += `
            <div class="card m-4">
                <img src="${i.img}" class="card-img-top">
                <div class="card-body">
                    <h3 class="card-title">${i.name}</h3>
                    <p class="card-text">${i.level}</p>
                    <button type="button" class="btn btn-sm btn-secondary w-100" data-bs-toggle="modal" data-bs-target="#mainModal" data-bs-nombre='${i.name}' data-bs-nivel='${i.level}' data-bs-imagen='${i.img}'>ver ${i.name}</button>
                </div>
            </div>
        `
    }
});
